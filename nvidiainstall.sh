#!/bin/bash

echo
echo "************************************************************************"
echo "**                                                                    **"
echo "**                           NVIDIAINSTALL                            **"  
echo "**                                                                    **"
echo "************************************************************************"
echo
echo "Este es el script de Roberto Díaz, para instalar el driver privativo de NVIDIA en tu distribución" 
echo "Y es su 4º versión"
echo
sleep 2s
echo "Vamos a ello......"
echo ""

# Obtener información de la distribución
source /etc/os-release
ID=$(echo "$ID" | tr '[:upper:]' '[:lower:]')

echo "Comprobaré que distribución usas e instalaré el driver de NVIDIA:"
sleep 1s

case "$ID" in
  debian)
    echo "Tu distribución es Debian"
    sudo apt install -y nvidia-detect
    driver=$(nvidia-detect | grep "nvidia-" | sed 's/ //g')
    sudo apt install -y "$driver"
    ;;
  
  ubuntu | linuxmint | zorinos | popos | kdeneon)
    echo "Tu distribución es $NAME"
    sudo ubuntu-drivers autoinstall
    ;;

  manjaro)
    echo "Tu distribución es Manjaro"
    sudo mhwd -a pci nonfree 0300
    ;;

  fedora)
    echo "Tu distribución es Fedora"
    sudo dnf install -y dnf-plugins-core
    sudo dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf install -y https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
    sudo dnf update --refresh
    sudo dnf install -y akmod-nvidia xorg-x11-drv-nvidia-cuda
    ;;

  arch)
    echo "Tu distribución es Arch Linux"
    sudo pacman -Syu nvidia-dkms nvidia-settings --noconfirm
    ;;

  *)
    echo "Distribución no reconocida o no soportada. Por favor, revisa la compatibilidad del script."
    exit 1
    ;;
esac

echo
echo "Este script cumple las cuatro libertades del software libre, que son las siguientes:"
echo ""
echo "1. Libertad de ejecutar el software como te plazca."
echo "2. Libertad de estudiar cómo funciona y modificarlo."
echo "3. Libertad de redistribuir copias del programa."
echo "4. Libertad de distribuir tus mejoras."
sleep 3s

echo
echo "Gracias por usar mi script."
echo "Deberías reiniciar el sistema para que los módulos de NVIDIA se carguen en el kernel."
echo
sleep 3s

read -p "¿Quieres reiniciar ahora (s/n)? " sn
case $sn in
  [Ss]* ) sudo reboot;;
  [Nn]* ) exit;;
  * ) echo "Por favor, pulsa s o n.";;
esac